package com.jmapps.lato.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class GenericException extends Exception {

    public GenericException(String message, Throwable ex, Object... params) {
        super(message, ex);
        StringBuilder sb = new StringBuilder(message);
        for (Object param : params) {
            sb.append(";").append(param);
        }
        log.error(sb.toString(), ex);
    }
}
