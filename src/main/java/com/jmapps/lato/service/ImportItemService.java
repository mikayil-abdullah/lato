package com.jmapps.lato.service;

import com.jmapps.lato.dao.ImportItemRepo;
import com.jmapps.lato.domain.model.ImportItem;
import org.springframework.stereotype.Service;

@Service
public class ImportItemService {
    private ImportItemRepo repo;

    public ImportItemService(ImportItemRepo repo) {
        this.repo = repo;
    }



    public ImportItem saveItem(ImportItem item) {
        return this.repo.save(item);
    }
}
