package com.jmapps.lato.service;

import com.jmapps.lato.dao.WarehouseRepo;
import com.jmapps.lato.domain.model.Warehouse;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class WarehouseService {
    private WarehouseRepo repo;

    public WarehouseService(WarehouseRepo repo) {
        this.repo = repo;
    }
    public Warehouse getWarehouse(Long id){
        return this.repo.findById(id).orElse(null);

    }
    public Iterable<Warehouse> getAllWarehouses() {
        return this.repo.findAll(Sort.by("name"));
    }

    public Iterable<Warehouse> getWarehouseByNameMatching(String match) {
        return this.repo.findAllByNameContainingIgnoreCase(match);
    }

    public Warehouse saveWarehouses(Warehouse warehouse) {
        return this.repo.save(warehouse);
    }

}
