package com.jmapps.lato.service;

import com.jmapps.lato.dao.PurchaseOrderRepo;
import com.jmapps.lato.domain.model.Client;
import com.jmapps.lato.domain.model.PurchaseOrder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class PurchaseOrderService {
    private PurchaseOrderRepo repo;

    public PurchaseOrderService(PurchaseOrderRepo repo) {
        this.repo = repo;
    }

    public Iterable<PurchaseOrder> getOrdersWithinPeriod(LocalDateTime start, LocalDateTime end) {
        return repo.findByOrderDateBetween(start, end);
    }

    public Iterable<PurchaseOrder> getOrdersWithinPeriodBy(LocalDateTime start, LocalDateTime end, Client client) {
        return repo.findByOrderDateBetweenAndClient(start, end, client);
    }

    public Iterable<PurchaseOrder> getOrdersOnDateBy(LocalDateTime orderDate, Client client) {
        return repo.findByOrderDateAndClient(orderDate, client);
    }

    public Iterable<PurchaseOrder> getOrdersOnDate(LocalDateTime orderDate) {
        return repo.findByOrderDate(orderDate);
    }

    public Iterable<PurchaseOrder> getOrdersByClient(Client client) {
        return repo.findByClient(client);
    }

    public PurchaseOrder getOrder(Long id) {
        return repo.findById(id).orElse(null);
    }

    public PurchaseOrder savePurchaseOrder(PurchaseOrder order) {
        return repo.save(order);
    }


}
