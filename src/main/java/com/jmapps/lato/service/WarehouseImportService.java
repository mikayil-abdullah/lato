package com.jmapps.lato.service;

import com.jmapps.lato.dao.WarehouseImportRepo;
import com.jmapps.lato.domain.model.WarehouseImport;
import org.springframework.stereotype.Service;

@Service
public class WarehouseImportService {
    private WarehouseImportRepo repo;



    public WarehouseImportService(WarehouseImportRepo repo) {
        this.repo = repo;
    }

    public WarehouseImport saveImport(WarehouseImport warehouseImport){

        return this.repo.save(warehouseImport);
    }

    public WarehouseImport getImportById(Long id){
        return this.repo.findById(id).orElse(null);
    }


}
