package com.jmapps.lato.service;

import com.jmapps.lato.dao.ClientRepo;
import com.jmapps.lato.domain.model.Client;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ClientService {
    private ClientRepo repo;

    public ClientService(ClientRepo repo) {
        this.repo = repo;
    }

    public Client getClient(Long id) {
        return this.repo.findById(id).orElse(null);
    }

    public Iterable<Client> getAllClients() {
        return this.repo.findAll(Sort.by("name"));
    }

    public Iterable<Client> getClientsByNameMatching(String match) {
        return this.repo.findClientsByNameContainingIgnoreCase(match);
    }

    public Client saveClient(Client client) {
        return this.repo.save(client);
    }
}
