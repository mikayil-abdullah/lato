package com.jmapps.lato.service;

import com.jmapps.lato.dao.SupplierRepo;
import com.jmapps.lato.domain.model.Supplier;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


@Service
public class SupplierService {
    private SupplierRepo repo;

    public SupplierService(SupplierRepo repo) {
        this.repo = repo;
    }

    public Supplier getSupplier(Long id){
        return this.repo.findById(id).orElse(null);
    }
    public Iterable<Supplier> getAllSuppliers() {
        return this.repo.findAll(Sort.by("name"));
    }

    public Iterable<Supplier> getSupplierByNameMatching(String match) {
        return this.repo.findAllByNameContainingIgnoreCase(match);
    }

    public Supplier saveSupplier(Supplier supplier) {
        return this.repo.save(supplier);
    }
}
