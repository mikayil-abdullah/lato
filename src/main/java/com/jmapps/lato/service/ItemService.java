package com.jmapps.lato.service;

import com.jmapps.lato.dao.ItemRepo;
import com.jmapps.lato.domain.model.Item;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ItemService {
    private ItemRepo repo;

    public ItemService(ItemRepo repo) {
        this.repo = repo;
    }

    public Item getItem(Long id){
        return this.repo.findById(id).orElse(null);

    }
    public Iterable<Item> getAllItems() {
        return this.repo.findAll(Sort.by("name"));
    }

    public Iterable<Item> getItemByNameMatching(String match) {
        return this.repo.findAllByNameContainingIgnoreCase(match);
    }

    public Item saveItem(Item item) {
        return this.repo.save(item);
    }

}
