package com.jmapps.lato.security;

public final class SecurityConstants {
    private SecurityConstants(){}

    public static final String USERNAME_HEADER = "X-Auth-Username";
    public static final String PASSWORD_HEADER = "X-Auth-Password";
    public static final String TOKEN_HEADER = "X-Auth-Token";
    public static final String VERSION_HEADER = "X-Client-Version";

}
