package com.jmapps.lato.controller;

import com.jmapps.lato.domain.model.PurchaseOrder;
import com.jmapps.lato.service.PurchaseOrderService;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/purchaseorders")
public class PurchseOrderController {
    private static final String DATE_MODE_BETWEEN = "between";
    private static final String DATE_MODE_BEFORE = "before";
    private static final String DATE_MODE_AFTER = "after";
    private static final String DATE_MODE_ON = "on";


    private PurchaseOrderService service;

    public PurchseOrderController(PurchaseOrderService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public PurchaseOrder getOrder(@PathVariable("id") Long id) {
        return service.getOrder(id);
    }

    @PostMapping
    public PurchaseOrder placeOrder(@RequestBody PurchaseOrder order) {
        return service.savePurchaseOrder(order);
    }

    @GetMapping
    public Iterable<PurchaseOrder> getOrders(@RequestParam(value = "clientid", required = false) Long clientId,
                                             @RequestParam(value = "datemode", required = false) String dateMode,
                                             @RequestParam(value = "startdate", required = false) LocalDateTime startDate,
                                             @RequestParam(value = "enddate", required = false) LocalDateTime endDate) {
        return null;//TODO continue
    }




}
