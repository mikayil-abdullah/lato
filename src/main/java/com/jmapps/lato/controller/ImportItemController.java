package com.jmapps.lato.controller;

import com.jmapps.lato.domain.model.ImportItem;
import com.jmapps.lato.service.ImportItemService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/importitems")
public class ImportItemController {
    private ImportItemService service;

    public ImportItemController(ImportItemService service) {
        this.service = service;
    }

    @PostMapping
    public ImportItem saveImportItem(@RequestBody ImportItem item) {
        return this.service.saveItem(item);
    }

}
