package com.jmapps.lato.controller;

import com.jmapps.lato.domain.model.Client;
import com.jmapps.lato.domain.model.PurchaseOrder;
import com.jmapps.lato.service.ClientService;
import com.jmapps.lato.service.PurchaseOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/clients")
@Slf4j
public class ClientController {

    private ClientService clientService;
    private PurchaseOrderService purchaseOrderService;

    public ClientController(ClientService clientService, PurchaseOrderService purchaseOrderService) {
        this.clientService = clientService;
        this.purchaseOrderService = purchaseOrderService;
    }

    @PostMapping
    public void saveClient(@RequestBody Client client) {
        this.clientService.saveClient(client);
    }

    @GetMapping
    public Iterable<Client> getClients(@RequestParam(required = false) String name) {
        if (name != null) {
            return this.clientService.getClientsByNameMatching(name);
        } else {
            return this.clientService.getAllClients();
        }
    }

    @GetMapping("/{id}")
    public Client getClient(@PathVariable("id") Long id) {
        return this.clientService.getClient(id);
    }

    @GetMapping("/{id}/purchaseorders")
    public List<PurchaseOrder> getOrdersByClient(@PathVariable("id") Long id) {
        Client client = clientService.getClient(id);
        if (client != null) {
            log.info("Client found with id {}. Now getting the client's purchase orders {}", id, client);
            return client.getPurchaseOrders();
        } else {
            log.warn("No client found with id {}. Returning null.", id);
            return null;
        }
    }

}
