package com.jmapps.lato.controller;

import com.jmapps.lato.domain.model.Item;
import com.jmapps.lato.service.ItemService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/items")
public class ItemController {
    private ItemService service;

    public ItemController(ItemService service) {
        this.service = service;
    }

    @PostMapping
    public void saveItem(@RequestBody Item item) {
        this.service.saveItem(item);
    }

    @GetMapping
    public Iterable<Item> getItems(@RequestParam(required = false) String name) {
        if (name != null) {
            return this.service.getItemByNameMatching(name);
        } else {
            return this.service.getAllItems();
        }
    }

    @GetMapping("/{id}")
    public Item getItem(@PathVariable("id") Long id) {
        return this.service.getItem(id);
    }
}
