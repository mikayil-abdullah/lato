package com.jmapps.lato.controller;


import com.jmapps.lato.domain.model.WarehouseImport;
import com.jmapps.lato.service.WarehouseImportService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/warehouse/imports")
public class WarehouseImportController {
    private WarehouseImportService warehouseImportService;

    public WarehouseImportController(WarehouseImportService warehouseImportService) {
        this.warehouseImportService = warehouseImportService;
    }

    @PostMapping
    public WarehouseImport importToWarehouse(@RequestBody WarehouseImport warehouseImport) {
        return warehouseImportService.saveImport(warehouseImport);
    }

    @GetMapping("/{id}")
    public WarehouseImport getImport(@PathVariable("id") Long id) {
        return warehouseImportService.getImportById(id);
    }

}
