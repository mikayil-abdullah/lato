package com.jmapps.lato.controller;


import com.jmapps.lato.domain.model.Supplier;
import com.jmapps.lato.service.SupplierService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/suppliers")
public class SupplierController {

    private SupplierService service;

    public SupplierController(SupplierService service) {
        this.service = service;
    }

    @PostMapping
    public void saveSupplier(@RequestBody Supplier supplier) {
        this.service.saveSupplier(supplier);
    }

    @GetMapping
    public Iterable<Supplier> getSuppliers(@RequestParam(required = false) String name) {
        if (name != null) {
            return this.service.getSupplierByNameMatching(name);
        } else {
            return this.service.getAllSuppliers();
        }
    }

    @GetMapping("/{id}")
    public Supplier getSupplier(@PathVariable("id") Long id) {
        return this.service.getSupplier(id);
    }
}
