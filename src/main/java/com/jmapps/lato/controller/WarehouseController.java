package com.jmapps.lato.controller;

import com.jmapps.lato.domain.model.Warehouse;
import com.jmapps.lato.service.WarehouseService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("warehouse")
public class WarehouseController {
    private WarehouseService service;

    public WarehouseController(WarehouseService service) {
        this.service = service;
    }

    @PostMapping
    public void saveWarehouse(@RequestBody Warehouse warehouse) {
        this.service.saveWarehouses(warehouse);
    }

    @GetMapping
    public Iterable<Warehouse> getWarehouses(@RequestParam(required = false) String name) {
        if (name != null) {
            return this.service.getWarehouseByNameMatching(name);
        } else {
            return this.service.getAllWarehouses();
        }
    }

    @GetMapping("/{id}")
    public Warehouse getWarehouse(@PathVariable("id") Long id) {
        return this.service.getWarehouse(id);
    }
}
