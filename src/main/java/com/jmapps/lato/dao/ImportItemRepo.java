package com.jmapps.lato.dao;

import com.jmapps.lato.domain.model.ImportItem;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ImportItemRepo extends PagingAndSortingRepository<ImportItem,Long> {
}
