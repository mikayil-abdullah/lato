package com.jmapps.lato.dao;

import com.jmapps.lato.domain.model.Role;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleRepo extends PagingAndSortingRepository<Role,Long>{

}
