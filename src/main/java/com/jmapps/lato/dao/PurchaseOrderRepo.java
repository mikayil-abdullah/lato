package com.jmapps.lato.dao;

import com.jmapps.lato.domain.model.Client;
import com.jmapps.lato.domain.model.PurchaseOrder;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDateTime;

public interface PurchaseOrderRepo extends PagingAndSortingRepository<PurchaseOrder, Long> {
    Iterable<PurchaseOrder> findByOrderDateBetween(LocalDateTime start, LocalDateTime end);

    Iterable<PurchaseOrder> findByOrderDateBetweenAndClient(LocalDateTime start, LocalDateTime end, Client client);

    Iterable<PurchaseOrder> findByOrderDateAndClient(LocalDateTime orderDate, Client client);

    Iterable<PurchaseOrder> findByOrderDate(LocalDateTime orderDate);

    Iterable<PurchaseOrder> findByClient(Client client);

}
