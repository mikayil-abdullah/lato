package com.jmapps.lato.dao;

import com.jmapps.lato.domain.model.Item;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ItemRepo extends PagingAndSortingRepository<Item,Long> {
    Iterable<Item> findAllByNameContainingIgnoreCase(String match);
}
