package com.jmapps.lato.dao;

import com.jmapps.lato.domain.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepo extends PagingAndSortingRepository<User,Long>{

}
