package com.jmapps.lato.dao;

import com.jmapps.lato.domain.model.Client;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ClientRepo extends PagingAndSortingRepository<Client, Long> {
    Iterable<Client> findClientsByNameContainingIgnoreCase(String match);
}
