package com.jmapps.lato.dao;

import com.jmapps.lato.domain.model.Supplier;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SupplierRepo extends PagingAndSortingRepository<Supplier,Long>{
    Iterable<Supplier> findAllByNameContainingIgnoreCase(String match);
}
