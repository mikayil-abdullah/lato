package com.jmapps.lato.dao;

import com.jmapps.lato.domain.model.Warehouse;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WarehouseRepo extends PagingAndSortingRepository<Warehouse,Long> {
    Iterable<Warehouse> findAllByNameContainingIgnoreCase(String match);
}
