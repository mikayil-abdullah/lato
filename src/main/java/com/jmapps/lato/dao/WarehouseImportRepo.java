package com.jmapps.lato.dao;

import com.jmapps.lato.domain.model.WarehouseImport;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WarehouseImportRepo extends PagingAndSortingRepository<WarehouseImport,Long> {
}
