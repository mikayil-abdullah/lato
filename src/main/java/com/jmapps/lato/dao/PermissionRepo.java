package com.jmapps.lato.dao;

import com.jmapps.lato.domain.model.Permission;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PermissionRepo extends PagingAndSortingRepository<Permission,Long> {
}
