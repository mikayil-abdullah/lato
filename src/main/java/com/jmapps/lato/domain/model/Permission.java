package com.jmapps.lato.domain.model;

import lombok.Data;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="permissions")
@Data
public class Permission {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "permission_id_generator")
    @SequenceGenerator(name = "permission_id_generator",sequenceName = "permission_id_seq")
    private Long id;
    @Column(unique = true, nullable = false, length = 100)
    @NaturalId
    private String name;
    @Column(length = 150)
    private String description;
    @ManyToMany(mappedBy = "permissions")
    private Set<Role> roles = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Permission)) return false;
        if (!super.equals(o)) return false;

        Permission that = (Permission) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}
