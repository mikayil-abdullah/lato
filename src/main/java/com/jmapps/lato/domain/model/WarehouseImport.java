package com.jmapps.lato.domain.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Meant for item import acctions.
 */
@Entity
@Table(name = "warehouse_imports")
@Data
public class WarehouseImport {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "warehouse_import_id_generator")
    @SequenceGenerator(name = "warehouse_import_id_generator",sequenceName = "warehouse_import_id_seq")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "supplier_id")
    private Supplier supplier;
    @Column(nullable = false)
    private ZonedDateTime importDate;
    @Column(nullable = false)
    private ZonedDateTime addedDate = ZonedDateTime.now();
    @ManyToOne
    @JoinColumn(name = "warehouse_id", referencedColumnName = "id")
    private Warehouse warehouse;
    @Setter(AccessLevel.NONE)
    @Transient
    private BigDecimal importSum;
    @OneToMany(mappedBy = "warehouseImport")
    private List<ImportItem> items = new ArrayList<>();

    public BigDecimal getImportSum() {
        return importSum;
    }
}
