package com.jmapps.lato.domain.model;

import com.jmapps.lato.dao.ItemRepo;
import com.jmapps.lato.dao.PurchaseOrderRepo;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "purchase_orders")
@Component
public class PurchaseOrder implements CommandLineRunner {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "purchase_order_id_generator")
    @SequenceGenerator(name = "purchase_order_id_generator", sequenceName = "purchase_order_id_seq")
    private Long id;
    @Column(nullable = false)
    private ZonedDateTime orderDate = ZonedDateTime.now();
    @Column(nullable = false)
    private ZonedDateTime addedDate = ZonedDateTime.now();
    @Column(length = 20)
    private String mobileNumber;
    @Column(length = 15)
    private String vehicleNumber;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private Client client;
    @OneToMany(mappedBy = "purchaseOrder", cascade = CascadeType.ALL)
    private List<PurchaseOrderItem> orderItems = new ArrayList<>();

    public void addOrderItem(PurchaseOrderItem item) {
        orderItems.add(item);
        item.setPurchaseOrder(this);
    }

    public void removeOrderItem(PurchaseOrderItem item) {
        orderItems.remove(item);
        item.setPurchaseOrder(null);
    }

    @Autowired
    @Transient
    PurchaseOrderRepo repo;
    @Transient
    @Autowired
    ItemRepo itemRepo;

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        PurchaseOrder order = new PurchaseOrder();
        Client client = new Client();
        client.setName("Mikola");
        order.setClient(client);
        order.setOrderDate(ZonedDateTime.now());


        Item item = new Item();
        item.setName("Profil1");
        itemRepo.save(item);
        Item item2 = new Item();
        item2.setName("Profil2");
        itemRepo.save(item2);

        PurchaseOrderItem orderItem = new PurchaseOrderItem();
        orderItem.setItem(item);
        orderItem.setQuantity(new BigDecimal(5));

        PurchaseOrderItem orderItem2 = new PurchaseOrderItem();
        orderItem2.setItem(item2);
        orderItem2.setQuantity(new BigDecimal(10));

        order.getOrderItems().add(orderItem);
        orderItem.setPurchaseOrder(order);
        order.getOrderItems().add(orderItem2);


        order.addOrderItem(orderItem);
        order.addOrderItem(orderItem2);
        repo.save(order);

    }
}
