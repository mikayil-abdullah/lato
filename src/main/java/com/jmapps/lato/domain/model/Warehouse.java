package com.jmapps.lato.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "warehouses")
@Data
public class Warehouse {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "warehouse_id_generator")
    @SequenceGenerator(name = "warehouse_id_generator",sequenceName = "warehouse_id_seq")
    private Long id;
    @NaturalId
    @Column(nullable = false, length = 70)
    private String name;
    @Column(length = 150)
    private String description;
    private ZonedDateTime addedDate = ZonedDateTime.now();
    @JsonIgnore
    @OneToMany(mappedBy = "warehouse")
    private List<WarehouseImport> imports = new ArrayList<>();
}
