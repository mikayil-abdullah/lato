package com.jmapps.lato.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "clients")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_id_generator")
    @SequenceGenerator(name = "client_id_generator", sequenceName = "client_id_seq")
    private Long id;
    @Column(nullable = false)
    private ZonedDateTime addedDate = ZonedDateTime.now();
    @Column(length = 100)
    private String name;
    @Column(length = 150)
    private String description;
    @Embedded
    private Contacts contacts;
    @OneToMany(mappedBy = "client")
    private List<PurchaseOrder> purchaseOrders = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Client)) return false;
        if (!super.equals(o)) return false;

        Client client = (Client) o;

        return id.equals(client.id);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + id.hashCode();
        return result;
    }
}
