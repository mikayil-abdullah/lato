package com.jmapps.lato.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "items")
@Data
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "item_id_generator")
    @SequenceGenerator(name="item_id_generator", sequenceName = "item_id_seq")
    private Long id;
    @Column(nullable = false, length = 100)
    private String name;
    private BigDecimal packSize;
    @Column(length = 150)
    private String description;
}
