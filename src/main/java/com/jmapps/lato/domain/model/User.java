package com.jmapps.lato.domain.model;

import lombok.Data;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

@Entity
@Table(name="users")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "user_id_generator")
    @SequenceGenerator(name = "user_id_generator",sequenceName = "user_id_seq")
    private Long id;
    @Column(unique = true, nullable = false, length = 50)
    @NaturalId
    private String username;
    @Column(nullable = false, length = 30)
    private String password;
    @Column(nullable = false, length = 50)
    private String firstName;
    @Column(nullable = false, length = 50)
    private String lastName;
    @Column(length = 100)
    private String email;
}
