package com.jmapps.lato.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "suppliers")
@Data
public class Supplier {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "supplier_id_generator")
    @SequenceGenerator(name = "supplier_id_generator",sequenceName = "supplier_id_seq")
    private Long id;
    @Column(nullable = false)
    private ZonedDateTime addedDate = ZonedDateTime.now();
    @Column(length = 100)
    private String name;
    @Column(length = 150)
    private String description;
    @Embedded
    private Contacts contacts;
    @JsonIgnore
    @OneToMany(mappedBy = "supplier")
    private List<WarehouseImport> imports = new ArrayList<>();

}
