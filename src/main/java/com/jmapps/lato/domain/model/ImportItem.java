package com.jmapps.lato.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "import_items")
@Data
public class ImportItem {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "import_item_id_generator")
    @SequenceGenerator(name="import_item_id_generator", sequenceName = "import_item_id_seq")
    private Long id;
    @OneToOne
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    private Item item;
    @Column(nullable = false)
    private BigDecimal quantity;
    @Column(nullable = false)
    private BigDecimal price;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "warehouse_import_id", referencedColumnName = "id")
    private WarehouseImport warehouseImport;
}
