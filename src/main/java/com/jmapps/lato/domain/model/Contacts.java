package com.jmapps.lato.domain.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Data
public class Contacts {
    @Column(length = 150)
    private String address;
    @Column(length = 20)
    private String workNumber;
    @Column(length = 20)
    private String mobileNumber;
}
