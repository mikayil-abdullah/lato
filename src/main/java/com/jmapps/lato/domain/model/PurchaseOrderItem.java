package com.jmapps.lato.domain.model;

import com.jmapps.lato.dao.ClientRepo;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@Entity
@Table(name = "purchase_order_items")
@Component
public class PurchaseOrderItem implements CommandLineRunner {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "purchase_order_item_id_generator")
    @SequenceGenerator(name = "purchase_order_item_id_generator", sequenceName = "purchase_order_item_id_seq")
    private Long id;
    @Column(nullable = false)
    private ZonedDateTime addedDate = ZonedDateTime.now();
    @OneToOne
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    private Item item;
    private BigDecimal quantity;
    @Column(length = 150)
    private String note;
    @ManyToOne
    @JoinColumn(name = "purchase_order_id", referencedColumnName = "id")
    private PurchaseOrder purchaseOrder;


    @Autowired
    @Transient
    ClientRepo repo;


    @Override
    @Transactional
    public void run(String... args) throws Exception {
        Client client = repo.findById(1L).get();
        for (PurchaseOrder order : client.getPurchaseOrders()) {
            System.out.println(order.getOrderDate());
        }
    }
}
